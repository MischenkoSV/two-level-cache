package ru.smishchenko.tlcache;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ru.smishchenko.cache.Cache;
import ru.smishchenko.cache.OneLevelCache;
import ru.smishchenko.cache.TwoLevelCache;
import ru.smishchenko.cache.storage.FileStorage;
import ru.smishchenko.cache.storage.MemoryStorage;
import ru.smishchenko.cache.strategy.LeastFrequentlyUsed;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class LfuCacheTest {
    Cache<String, String> cache;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new OneLevelCache(new MemoryStorage(), new LeastFrequentlyUsed(), 5)},
                {new OneLevelCache(new FileStorage(), new LeastFrequentlyUsed(), 5)},
                {new TwoLevelCache(new LeastFrequentlyUsed(), new LeastFrequentlyUsed(), 2, 3)}
        });
    }

    public LfuCacheTest(Cache<String, String> cache) {
        this.cache = cache;
    }

    @After
    public void clearCache() {
        cache.clear();
    }

    @Test
    public void testMoveObjectFromCache() {
        cache.put("1", "1");
        cache.put("2", "2");
        cache.put("3", "3");
        cache.put("4", "4");
        cache.put("5", "5");
        assertTrue(cache.containsKey("1"));
        assertTrue(cache.containsKey("2"));
        assertTrue(cache.containsKey("3"));
        assertTrue(cache.containsKey("4"));
        assertTrue(cache.containsKey("5"));
        cache.get("1");
        cache.get("1");
        cache.get("1");
        cache.get("3");
        cache.get("3");
        cache.get("4");
        cache.get("4");
        cache.put("6", "6");
        cache.get("6");
        cache.put("7", "7");
        assertTrue(cache.containsKey("1"));
        assertFalse(cache.containsKey("2"));
        assertTrue(cache.containsKey("3"));
        assertTrue(cache.containsKey("4"));
        assertFalse(cache.containsKey("5"));
        assertTrue(cache.containsKey("6"));
        assertTrue(cache.containsKey("7"));
    }
}
