package ru.smishchenko.cache;

import javafx.util.Pair;

public interface Cache<K, V> {

    V get(K key);

    boolean containsKey(K key);

    void put(K key, V value);

    Pair<K, V> putAndReturnReplacement(K key, V value);

    boolean remove(K key);

    void clear();

    int size();
}
