package ru.smishchenko.cache;

import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.smishchenko.cache.storage.FileStorage;
import ru.smishchenko.cache.storage.MemoryStorage;
import ru.smishchenko.cache.strategy.Strategy;

import static java.util.Objects.requireNonNull;

public class TwoLevelCache<K, V> implements Cache<K, V> {
    private static final Logger log = LoggerFactory.getLogger(OneLevelCache.class);

    int firstLevelSize;
    int secondLevelSize;
    private final OneLevelCache<K, V> firstLevelCache;
    private final OneLevelCache<K, V> secondLevelCache;

    public TwoLevelCache(Strategy<K> firstLevelStrategy, Strategy<K> secondLevelStrategy, int firstLevelSize, int secondLevelSize) {
        this.firstLevelSize = firstLevelSize;
        this.secondLevelSize = secondLevelSize;
        this.firstLevelCache = new OneLevelCache(new MemoryStorage(), firstLevelStrategy, firstLevelSize);
        this.secondLevelCache = new OneLevelCache(new FileStorage(), secondLevelStrategy, secondLevelSize);
    }

    @Override
    public V get(K key) {
        requireNonNull(key);
        return firstLevelCache.containsKey(key) ? firstLevelCache.get(key) : secondLevelCache.get(key);
    }

    @Override
    public boolean containsKey(K key) {
        requireNonNull(key);
        return firstLevelCache.containsKey(key) ? true : secondLevelCache.containsKey(key);
    }

    @Override
    public void put(K key, V value) {
        putAndReturnReplacement(key, value);
    }

    @Override
    public Pair<K, V> putAndReturnReplacement(K key, V value) {
        requireNonNull(key);
        requireNonNull(value);
        Pair<K, V> replacement = firstLevelCache.putAndReturnReplacement(key, value);
        if (replacement == null) {
            return null;
        } else {
            log.debug("Move key {} value {} from firs level to second level", replacement.getKey(), replacement.getValue());
            return secondLevelCache.putAndReturnReplacement(replacement.getKey(), replacement.getValue());
        }
    }

    @Override
    public boolean remove(K key) {
        requireNonNull(key);
        return firstLevelCache.containsKey(key) ? firstLevelCache.remove(key) : secondLevelCache.remove(key);
    }

    @Override
    public void clear() {
        firstLevelCache.clear();
        secondLevelCache.clear();
    }

    @Override
    public int size() {
        return firstLevelSize + secondLevelSize;
    }

    @Override
    public String toString() {
        return "TwoLevelCache{" +
                "firstLevelCache=" + firstLevelCache +
                ", secondLevelCache=" + secondLevelCache +
                '}';
    }
}
