package ru.smishchenko.cache.storage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import static java.util.Objects.requireNonNull;

public class FileStorage<K, V> implements Storage<K, V> {
    private final HashMap<K, String> fileNameStorage;
    private final Path cacheDir;

    public FileStorage() {
        this.fileNameStorage = new HashMap();
        try {
            this.cacheDir = Files.createTempDirectory("cache");
        } catch (IOException e) {
            throw new RuntimeException("Cache directory initialization error", e);
        }
    }

    @Override
    public V get(K key) {
        requireNonNull(key);
        String fileName = fileNameStorage.get(key);
        if (fileName == null) {
            return null;
        } else {
            try (FileInputStream fileInputStream = new FileInputStream(new File(cacheDir + File.separator + fileName));
                 ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
                return (V) objectInputStream.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException("Error reading file " + fileName, e);
            }
        }
    }

    @Override
    public boolean containsKey(K key) {
        requireNonNull(key);
        return fileNameStorage.containsKey(key);
    }

    @Override
    public void put(K key, V value) {
        requireNonNull(key);
        requireNonNull(value);
        File cacheFile;
        try {
            cacheFile = Files.createTempFile(cacheDir, "", "").toFile();
        } catch (IOException e) {
            throw new RuntimeException("Cache file creation error", e);
        }
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(cacheFile))) {
            outputStream.writeObject(value);
            outputStream.flush();
        } catch (IOException e) {
            throw new RuntimeException("Error writing to cache file", e);
        }
        fileNameStorage.put(key, cacheFile.getName());
    }

    @Override
    public boolean remove(K key) {
        requireNonNull(key);
        return fileNameStorage.remove(key) != null;
    }

    @Override
    public void clear() {
        try {
            Files.walk(cacheDir)
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    .forEach(File::delete);
            Files.delete(cacheDir);
        } catch (IOException e) {
            throw new RuntimeException("Cache directory clearing error", e);
        }
        fileNameStorage.clear();
    }

    @Override
    public int size() {
        return fileNameStorage.size();
    }
}
