package ru.smishchenko.cache.storage;

public interface Storage<K, V> {
    V get(K key);

    boolean containsKey(K key);

    void put(K key, V value);

    boolean remove(K key);

    void clear();

    int size();
}
