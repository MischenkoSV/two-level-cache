package ru.smishchenko.cache.storage;

import java.util.HashMap;

public class MemoryStorage<K, V> implements Storage<K, V> {
    private final HashMap<K, V> dataStorage;

    public MemoryStorage() {
        this.dataStorage = new HashMap();
    }

    @Override
    public V get(K key) {
        return dataStorage.get(key);
    }

    @Override
    public boolean containsKey(K key) {
        return dataStorage.containsKey(key);
    }

    @Override
    public void put(K key, V value) {
        dataStorage.put(key, value);
    }

    @Override
    public boolean remove(K key) {
        return dataStorage.remove(key) != null;
    }

    @Override
    public void clear() {
        dataStorage.clear();
    }

    @Override
    public int size() {
        return dataStorage.size();
    }
}
