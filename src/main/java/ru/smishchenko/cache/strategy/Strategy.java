package ru.smishchenko.cache.strategy;

public interface Strategy<K> {
    void put(K key);

    void get(K key);

    void remove(K key);

    void clear();

    int size();

    K getReplacementKey();
}
