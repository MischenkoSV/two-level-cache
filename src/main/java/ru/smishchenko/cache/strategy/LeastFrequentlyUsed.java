package ru.smishchenko.cache.strategy;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class LeastFrequentlyUsed<K> implements Strategy<K> {
    private final HashMap<K, Integer> strategyMap;

    public LeastFrequentlyUsed() {
        this.strategyMap = new HashMap();
    }

    @Override
    public void put(K key) {
        strategyMap.put(key, 0);
    }

    @Override
    public void get(K key) {
        Integer value = strategyMap.get(key);
        strategyMap.put(key, ++value);
    }

    @Override
    public void remove(K key) {
        strategyMap.remove(key);
    }

    @Override
    public void clear() {
        strategyMap.clear();
    }

    @Override
    public int size() {
        return strategyMap.size();
    }

    @Override
    public K getReplacementKey() {
        return strategyMap.entrySet().stream()
                .max(Comparator.comparing(Map.Entry<K, Integer>::getValue).reversed())
                .get().getKey();
    }

    @Override
    public String toString() {
        return strategyMap.toString();
    }
}
