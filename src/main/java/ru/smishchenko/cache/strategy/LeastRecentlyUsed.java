package ru.smishchenko.cache.strategy;

import java.util.LinkedHashSet;

public class LeastRecentlyUsed<K> implements Strategy<K> {
    private final LinkedHashSet<K> strategyStorage;

    public LeastRecentlyUsed() {
        this.strategyStorage = new LinkedHashSet();
    }

    @Override
    public void put(K key) {
        strategyStorage.add(key);
    }

    @Override
    public void get(K key) {
        strategyStorage.remove(key);
        strategyStorage.add(key);
    }

    @Override
    public void remove(K key) {
        strategyStorage.remove(key);
    }

    @Override
    public void clear() {
        strategyStorage.clear();
    }

    @Override
    public int size() {
        return strategyStorage.size();
    }

    @Override
    public K getReplacementKey() {
        return strategyStorage.iterator().next();
    }

    @Override
    public String toString() {
        return strategyStorage.toString();
    }
}
