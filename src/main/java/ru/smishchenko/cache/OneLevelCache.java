package ru.smishchenko.cache;

import javafx.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.smishchenko.cache.storage.Storage;
import ru.smishchenko.cache.strategy.Strategy;

import static java.util.Objects.requireNonNull;

public class OneLevelCache<K, V> implements Cache<K, V> {
    private static final Logger log = LoggerFactory.getLogger(OneLevelCache.class);
    private final Storage<K, V> storage;
    private final Strategy<K> strategy;
    private final int maxSize;

    public OneLevelCache(Storage<K, V> storage, Strategy<K> strategy, int maxSize) {
        this.storage = storage;
        this.strategy = strategy;
        this.maxSize = maxSize;
    }

    @Override
    public V get(K key) {
        requireNonNull(key);
        strategy.get(key);
        V value = storage.get(key);
        log.debug("Cache({}, {}). Get key {} value {}",
                storage.getClass().getSimpleName(), strategy.getClass().getSimpleName(), key, value);
        return value;
    }

    @Override
    public boolean containsKey(K key) {
        requireNonNull(key);
        return storage.containsKey(key);
    }

    @Override
    public void put(K key, V value) {
        putAndReturnReplacement(key, value);
    }

    @Override
    public Pair<K, V> putAndReturnReplacement(K key, V value) {
        requireNonNull(key);
        requireNonNull(value);
        Pair<K, V> replacement = null;
        if (storage.size() >= maxSize) {
            K replacementKey = strategy.getReplacementKey();
            replacement = new Pair(replacementKey, storage.get(replacementKey));
            strategy.remove(replacementKey);
            storage.remove(replacementKey);
            log.debug("Cache({}, {}). Remove key {} value {}",
                    storage.getClass().getSimpleName(), strategy.getClass().getSimpleName(), replacement.getKey(), replacement.getValue());
        }
        strategy.put(key);
        storage.put(key, value);
        log.debug("Cache({}, {}). Put key {} value {}",
                storage.getClass().getSimpleName(), strategy.getClass().getSimpleName(), key, value);
        return replacement;
    }

    @Override
    public boolean remove(K key) {
        requireNonNull(key);
        log.debug("Cache({}, {}). Remove key {}",
                storage.getClass().getSimpleName(), strategy.getClass().getSimpleName(), key);
        strategy.remove(key);
        return storage.remove(key);
    }

    @Override
    public void clear() {
        strategy.clear();
        storage.clear();
    }

    @Override
    public int size() {
        return storage.size();
    }

    @Override
    public String toString() {
        return strategy.toString();
    }
}
